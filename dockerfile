FROM nginx:latest

copy ./navigation.html /usr/share/nginx/html/index.html
copy ./*.css /usr/share/nginx/html
copy ./*.png /usr/share/nginx/html
